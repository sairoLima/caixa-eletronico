import java.util.Scanner;

import java.text.NumberFormat;
import java.util.Locale;

public class main {

    public static void espaco() {
        System.out.println("\n");
    }

    public static void main(String[] args) {

        Locale localeBR = new Locale("pt", "BR");

        NumberFormat formatoDinheiro = NumberFormat.getCurrencyInstance(localeBR);

        Scanner ler = new Scanner(System.in);

        String modelo_cpf = "123.456.789-00", modelo_senha = "01020304", senha, cpf;

        double saldo = 1000, entrada = 0;

        boolean autenticado = false;
        int selecao;

        System.out.println("====================== Bem Vindo ======================");
        System.out.println("============== Ao caixa eletrônico JAVA! ==============");

        System.out.println("Digite seu CPF e senha para começar.");

        do {
            System.out.print("Digite o CPF: ");

            cpf = ler.next();

            if (cpf.equals(modelo_cpf)) {

                System.out.print("Digite a senha: ");

                senha = ler.next();

                if (senha.equals(modelo_senha)) { // 01020304

                    autenticado = true;
                } else {
                    System.out.println("Senha inválida!");

                }

            } else {
                System.out.println("CPF inválido!");

            }

        } while (autenticado == false);

        System.out.println("Tudo aparenta estar certo, aqui está o menu:");
        System.out.println("Onde vai gastar hoje?");
        while (autenticado == true) {

            System.out.print("1 - Saldo\n2 - Depósito\n3 - Saque\n0 - Sair ");

            selecao = ler.nextInt();

            switch (selecao) {
                case 1:

                    System.out.println("Saldo: " + formatoDinheiro.format(saldo));
                    break;
                case 2:

                    System.out.println("Quanto deseja depositar? ");

                    entrada = ler.nextDouble();

                    saldo = saldo + entrada;

                    entrada = 0;
                    selecao = 0;

                    System.out.println("Novo saldo: " + formatoDinheiro.format(saldo));
                    break;
                case 3:

                    System.out.println("Quanto deseja retirar? ");
                    entrada = ler.nextDouble();
                    saldo = saldo - entrada;
                    entrada = 0;
                    selecao = 0;

                    System.out.println("Novo saldo: " + formatoDinheiro.format(saldo));

                    break;

                case 0:

                    System.out.println("Obrigado por nos dar seu dinheiro, volte sempre");
                    System.exit(0);
                    break;

                default:
                    System.out.println("Opção inválida.");
                    break;

            }
        }

        ler.close();
    }
}